import { Card, Button } from "react-bootstrap";

export default function CourseCard() {
   return (
      <>
         <Card className="m-3">
            <Card.Body>
               <Card.Title>
                  <h5>Sample Course</h5>
               </Card.Title>
               <Card.Text>
                  <h6 className="">Description:</h6>
                  <p className="pb-2">This is a sample course offering</p>
                  <h6 className="">Price:</h6>
                  <p className="pb-2">PHP 40,000</p>
               </Card.Text>
               <Button variant="primary">Enroll</Button>
            </Card.Body>
         </Card>
      </>
   );
}
