import "./App.css";
import AppNavbar from "./components/AppNavbar.js";
import Home from "./pages/Home.js";
import { Container } from "react-bootstrap";

function App() {
   return (
      // Fragment: <> and </>
      /* Mount component and prepare output rendering */
      <>
         <AppNavbar />
         <Container>
            <Home />
         </Container>
      </>
   );
}

export default App;
